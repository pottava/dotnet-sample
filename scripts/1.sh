#!/bin/sh
set -eu

jobid=$1
if [ -z "$jobid" ]; then
    echo "Job ID が指定されていません" >"error.log"
    exit 1
fi
test_dir=${TEST_DIRECTORY:-/test}

# Job ごとのフォルダを作る
mkdir -p "${jobid}"

# 2 番目の処理を呼び出す
echo -n "\nRun a building process\n" >"${jobid}/01.log"
echo "======" >>"${jobid}/01.log"
curl -s "${API_HOST:-api-host}:${PORT:-8080}/exec/%27%2Fscripts%2F2.sh%27/${jobid}" &

# モニタープロセスを起動する
echo -n "\nRun a monitoring process\n" >>"${jobid}/01.log"
echo "======" >>"${jobid}/01.log"
expected_folder_count=$(ls -d ${test_dir}/* -1U | wc -l)
echo "expected_folder_count: ${expected_folder_count}" >>"${jobid}/01.log" 2>&1

# 3 秒おきにフォルダ数が一致していることを確認
set +e
while :
do
    count=$(ls -d ${jobid}/*/ -1U | wc -l) 2>/dev/null
    echo "count: ${count}" >>"${jobid}/01.log" 2>&1
    if [ $count -eq $expected_folder_count ]; then
        break
    fi
    sleep 3
done

# 結果をまとめる
set -e
echo "Monitoring is done" >>"${jobid}/01.log" 2>&1
