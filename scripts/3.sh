#!/bin/sh
set -eu

jobid=$1
if [ -z "$jobid" ]; then
    echo "Job ID が指定されていません" >"error.log"
    exit 1
fi
folder=$2
if [ -z "$folder" ]; then
    echo "テストフォルダが指定されていません" >"error.log"
    exit 1
fi
test_dir=${TEST_DIRECTORY:-/test}

# テストごとのフォルダを作る
mkdir -p "${jobid}/${folder}"

# テスト実行
echo "Run tests" >"${jobid}/${folder}/03.log"
echo "======" >>"${jobid}/${folder}/03.log"
./${jobid}/sil >>"${jobid}/${folder}/03.log" 2>&1
if [ $? -ne 0 ]; then 
    exit 1
fi
cat "${test_dir}/${folder}/input.csv" >>"${jobid}/${folder}/03.log"

# 4 番目の処理を呼び出す
echo -n "\nRun a rendering process\n" >>"${jobid}/${folder}/03.log"
echo "======" >>"${jobid}/${folder}/03.log"
curl -s -H 'X-Application: python3' \
  "${API_HOST:-api-host}:${PORT:-8080}/exec/%27%2Fscripts%2F4.py%27/${jobid}/${folder}"
echo -n "done.\n\n" >>"${jobid}/${folder}/03.log"
