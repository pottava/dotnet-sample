import os
import sys


def render(jobid, folder):
    os.makedirs(jobid+'/'+folder, exist_ok=True)
    with open(jobid+'/'+folder+'/04.log', mode='w') as f:
        f.write('Rendering process has done.\n')


if __name__ == '__main__':
    args = sys.argv
    if 3 <= len(args):
        if args[1].isdigit():
            render(args[1], args[2])
        else:
            print('Argument should be digit')
    else:
        print('There are missing arguments')
        sys.exit(os.EX_USAGE)
    sys.exit(os.EX_OK)
