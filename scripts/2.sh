#!/bin/sh
set -eu

jobid=$1
if [ -z "$jobid" ]; then
    echo "Job ID が指定されていません" >"error.log"
    exit 1
fi
source_code=${SOURCE_CODE:-/sample/dummy.cpp}
test_dir=${TEST_DIRECTORY:-/test}

# Job ごとのフォルダを作る
mkdir -p "${jobid}"

# ビルドする
echo "Build a c++ library" >"${jobid}/02.log"
echo "======" >>"${jobid}/02.log"

g++ -o "${jobid}/sil" "${source_code}" >>"${jobid}/02.log" 2>&1
if [ $? -ne 0 ]; then 
    exit 1
fi
echo -n "done.\n\n" >>"${jobid}/02.log"

# テストフォルダの一覧を読み、3 番目の処理を呼び出す
echo "Run tests" >>"${jobid}/02.log"
echo "======" >>"${jobid}/02.log"
for folder in $(ls -d ${test_dir}/*/); do
    curl -s "${API_HOST:-api-host}:${PORT:-8080}/exec/%27%2Fscripts%2F3.sh%27/${jobid}/${folder#*${test_dir}/}" &
done
echo -n "done.\n\n" >>"${jobid}/02.log"
