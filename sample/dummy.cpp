#include <iostream>
#include <random>
#include <unistd.h>

int main(int argc, char *argv[])
{
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());
    std::uniform_int_distribution<> rnd(1, 5);

    int x = rnd(engine);
    int y = rnd(engine);
    std::cout << "sleep=" << x << ", exit=" << y << std::endl;

    int i;
    for (i=1; i<argc ;i++) {
        std::cout << "i: " << i << ", v=" << argv[i] << std::endl;
    }
    sleep(x);

    std::cout << "done." << std::endl;
    // if (y == 5) {
    //     return 1;
    // }
    return 0;
}
