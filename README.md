CI runner on GKE with GCS
---

Kubernetes を利用した CI テストランナーです。  
任意のコマンドを REST API でラップしてテストを実行します。

## CI ランナーの開発

### 1. コンテナをビルド

```sh
docker build -t api-server -f api-wrapper/Dockerfile .
```

### 2. コンテナを起動

Docker コンテナとして起動する例

```sh
docker run --rm -u $(id -u) --add-host=api-host:host-gateway \
    -v $(pwd):/work -v $(pwd)/scripts:/scripts \
    -v $(pwd)/sample:/sample -v $(pwd)/test:/test \
    -p 8080:8080 api-server
```

Skaffold を使い Kubernetes サービスとして起動する例

```sh
minikube start
skaffold dev -p local --port-forward
```

### 3. 外部からアクセス

```sh
curl -iXGET localhost:8080/exec/-c/%27ls%20-la%20%2Fscripts%27
curl -iXGET localhost:8080/exec/-c/%27cat%20%2Fscripts%2F4.py%27
curl -iXGET localhost:8080/exec/%27%2Fscripts%2F1.sh%27/1001
curl -iXGET localhost:8080/exec/%27%2Fscripts%2F2.sh%27/1001
curl -iXGET localhost:8080/exec/%27%2Fscripts%2F3.sh%27/1001/test01
curl -i -H "X-Application: python3" -XGET localhost:8080/exec/%27%2Fscripts%2F4.py%27/1001/test01
```

## Kubernetes へのデプロイ

### 1. ローカル クライアントの認証

```sh
gcloud auth login
gcloud config set project <your-project-id>
export resource_id=<your-id>
```

### 2. 利用サービスの有効化

```sh
gcloud services enable compute.googleapis.com container.googleapis.com \
    run.googleapis.com artifactregistry.googleapis.com \
    iamcredentials.googleapis.com
```

### 3. Artifact Registry にリポジトリを作成

```sh
gcloud artifacts repositories create "ci-imgs-${resource_id}" \
    --repository-format "docker" --location "asia-northeast1" \
    --description "Docker repository for ci apps"
```

### 4. バケットの作成

```sh
export project_id=$( gcloud config get-value project )
export bucket_name="ci-${project_id}-${resource_id}"
gcloud storage buckets create "gs://${bucket_name}" --location "asia-northeast1" \
    --uniform-bucket-level-access --public-access-prevention --enable-autoclass
```

### 5. VPC ネットワークの作成

```sh
gcloud compute networks create "ci-${resource_id}" --subnet-mode "custom"
gcloud compute networks subnets create "ci-tokyo-${resource_id}" \
    --network "ci-${resource_id}" --region "asia-northeast1" \
    --range "10.1.2.0/24" --enable-private-ip-google-access
gcloud compute networks subnets create "ci-proxy-${resource_id}" \
    --purpose "REGIONAL_MANAGED_PROXY" --role "ACTIVE" \
    --network "ci-${resource_id}" --region "asia-northeast1" \
    --range "10.129.0.0/23"
```

### 6. Cloud NAT の設置

```sh
gcloud compute addresses create "nat-ip-${resource_id}" --region "asia-northeast1"
gcloud compute routers create "router-ci-${resource_id}" --network "ci-${resource_id}" \
    --region "asia-northeast1"
gcloud compute routers nats create "nat-ci-${resource_id}" --region "asia-northeast1" \
    --router "router-ci-${resource_id}" --nat-custom-subnet-ip-ranges "ci-tokyo-${resource_id}" \
    --nat-external-ip-pool "nat-ip-${resource_id}"
```

### 7. GKE クラスタの作成

GKE Autopilot の場合

```sh
gcloud container clusters create-auto "ci-linux-${resource_id}" --release-channel "stable" \
    --network "ci-${resource_id}" --subnetwork "ci-tokyo-${resource_id}" --region "asia-northeast1" \
    --enable-private-nodes --no-enable-master-authorized-networks
gcloud container clusters get-credentials "ci-linux-${resource_id}" --location asia-northeast1
```

GKE Standard の場合

```sh
gcloud container clusters create "ci-linux-${resource_id}" --release-channel "stable" \
    --network "ci-${resource_id}" --subnetwork "ci-tokyo-${resource_id}" --zone "asia-northeast1-c" \
    --machine-type "e2-standard-4" --num-nodes 1 --min-nodes 1 --max-nodes 10 \
    --gateway-api "standard" --addons "HttpLoadBalancing,GcsFuseCsiDriver" \
    --enable-autoscaling --enable-autoprovisioning --max-cpu 50 --max-memory 256 \
    --enable-ip-alias --enable-private-nodes --master-ipv4-cidr 100.100.10.0/28 \
    --no-enable-master-authorized-networks --enable-image-streaming \
    --workload-pool "${project_id}.svc.id.goog"
gcloud container clusters get-credentials "ci-linux-${resource_id}" --zone asia-northeast1-c
```

### 8. アプリケーション用サービスアカウントを作成

クラウド側

```sh
gcloud iam service-accounts create "apis-sa-${resource_id}" \
    --display-name "SA for demo apis" \
    --description "Service Account for demo APIs"
gcloud storage buckets add-iam-policy-binding "gs://${bucket_name}" \
    --member "serviceAccount:apis-sa-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.admin"
gcloud iam service-accounts add-iam-policy-binding \
    "apis-sa-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --member "serviceAccount:${project_id}.svc.id.goog[default/apis-sa]" \
    --role roles/iam.workloadIdentityUser
```

Kubernetes 側

```sh
kubectl create serviceaccount apis-sa
kubectl annotate serviceaccount apis-sa \
    "iam.gke.io/gcp-service-account=apis-sa-${resource_id}@${project_id}.iam.gserviceaccount.com"
```

### 9. イメージのビルド

```sh
export repo="asia-northeast1-docker.pkg.dev/${project_id}/ci-imgs-${resource_id}"
gcloud auth configure-docker asia-northeast1-docker.pkg.dev
skaffold build --default-repo "${repo}" --push --file-output build.out
```

### 10. デプロイ

```sh
sed -i -e "s/BUCKET_NAME/${bucket_name}/g" deploy/k8s/overlays/cloud/persistent-volume.yaml
skaffold render -o cloud.yaml -p cloud --build-artifacts build.out
kubectl apply -f cloud.yaml
```

完了を待ちましょう

```sh
kubectl wait --for=condition=Ready pod --all --timeout "600s"
kubectl wait --all-namespaces gateways --all --for "condition=READY" --timeout "600s"
```

### 11. Service A への接続先を得る

```sh
cat << EOF

http://$( kubectl get gateways.gateway.networking.k8s.io ci-gateway \
    -o json | jq -r ".status.addresses[0].value" )/

EOF
```

## Kubernetes クラスタを GitLab CI/CD のランナーとして登録する

### CI ランナーのインストール

GitLab CI を Kubernetes 上で動かす設定はインターネット上さまざま見つかりますが、v16.5 時点においては Kubernetes 側に Runner のエージェントを起動するだけで CI が動き始めます。まずはプロジェクト固有のランナーを設定してみましょう。CI を設定する GitLab プロジェクトの "Settings > CI/CD" ページの "Runners" 設定を開き、Registration Token を確認、変数に設定します。

```sh
export registration_token=
```

CI ランナーをインストールします。

```sh
kubectl create namespace gitlab
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
helm inspect values gitlab/gitlab-runner > values.yaml
```

以下を参考に、values.yaml を修正します。  
仕様上、Autopilot クラスタの場合は privileged は false にする必要があります。

```
gitlabUrl: http://<GitLab のホスト名>/
runnerRegistrationToken: "${registration_token}"
rbac:
  create: true
runners:
  config: |
    [[runners]]
      executor = "kubernetes"

      [runners.kubernetes]
        image = "ubuntu:22.04"
        pull_policy = "if-not-present"
        poll_timeout = 900
        privileged = true

  tags: linux-privileged
```

設定を反映させます。

```sh
helm install -n gitlab gitlab-runner -f values.yaml gitlab/gitlab-runner
kubectl wait -n gitlab --for=condition=Ready pod --all --timeout "300s"
```

### キャッシュを設定する（オプション）

前のジョブで利用したコンテンツを再利用しジョブの実行時間を短縮することを目的に、キャッシュを導入します。まずキャッシュ用のバケットを GCS に作成します。

```sh
gcloud storage buckets create "gs://gitlab-cache-${project_id}-${resource_id}" --location "asia-northeast1" \
    --uniform-bucket-level-access --public-access-prevention --enable-autoclass
```

このバケットにのみ書き込み権限をもつサービスアカウントと、その鍵を作成します。

```sh
gcloud iam service-accounts create "gitlab-cache-${resource_id}" \
    --description "Service Account for the GitLab CI Cache" \
    --display-name "SA for GitLab CI Cache"
gcloud storage buckets add-iam-policy-binding "gs://gitlab-cache-${project_id}-${resource_id}" \
    --member "serviceAccount:gitlab-cache-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.objectCreator"
gcloud storage buckets add-iam-policy-binding "gs://gitlab-cache-${project_id}-${resource_id}" \
    --member "serviceAccount:gitlab-cache-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.objectViewer"
gcloud iam service-accounts keys create key.json \
    --iam-account "gitlab-cache-${resource_id}@${project_id}.iam.gserviceaccount.com"
kubectl create secret generic google-application-credentials -n gitlab \
    --from-file "gcs-application-credentials-file=key.json"
rm -rf key.json
```

Job の間でデータがやりとりできるよう、values.yaml にキャッシュを設定します。

```sh
runners:
  config: |
    [[runners]]
      executor = "kubernetes"

      [runners.kubernetes]
        image = "ubuntu:22.04"
        pull_policy = "if-not-present"
        poll_timeout = 900
        privileged = true

      [runners.cache]
        Type = "gcs"
        Path = "runner"
        Shared = true
        [runners.cache.gcs]
          BucketName = "gitlab-cache-${project_id}-${resource_id}"
  cache:
    secretName: google-application-credentials
secrets:
  - name: google-application-credentials
```

ランナーの設定を変更します。

```sh
helm upgrade -n gitlab gitlab-runner -f values.yaml gitlab/gitlab-runner
kubectl wait -n gitlab --for=condition=Ready pod --all --timeout "300s"
```

## Kind を利用した GKE-E クラスタの作成

GKE-E を利用して、任意の Kubernetes クラスタを CI ランナーとして登録してみます。

### 1. GKE Enterprise の有効化

```sh
gcloud services enable connectgateway.googleapis.com anthos.googleapis.com \
    gkeconnect.googleapis.com gkehub.googleapis.com gkemulticloud.googleapis.com \
    cloudresourcemanager.googleapis.com
```

### 2. VM の起動

VM を起動する VPC に NAT が起動していることを確認し  
その NAT を経由するよう、プライベート IP のみで VM を起動します。

```sh
gcloud compute instances create "kind-${resource_id}" --zone "asia-northeast1-c" \
    --machine-type "n2-standard-4" --no-address --scopes cloud-platform
```

### 3. SSH

IAP からの SSH を許可して、VM に SSH で入ります。

```sh
gcloud compute firewall-rules create allow-ssh-from-iap \
    --direction "INGRESS" --action "allow" --rules "tcp:22" \
    --source-ranges "35.235.240.0/20"
gcloud compute ssh --zone "asia-northeast1-c" "kind-${resource_id}" --tunnel-through-iap
```

### 4. ツールのインストール

Debian サーバー向けインストール手順を見て Docker をセットアップします。  
https://docs.docker.com/engine/install/debian/

Linux サーバー向けインストール手順を見て kind をセットアップします。  
https://kind.sigs.k8s.io/docs/user/quick-start/

Linux サーバー向けインストール手順を見て kubectl をセットアップします。  
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

### 5. kind クラスタの起動

```sh
cat << EOF >config.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  image: kindest/node:v1.27.0@sha256:c6b22e613523b1af67d4bc8a0c38a4c3ea3a2b8fbc5b367ae36345c9cb844518
EOF
kind create cluster --config config.yaml
```

### 6. GKE Enterprise によるクラスタ管理

kind クラスタを GKE-E クラスタとして連携します。

```sh
resource_id=
project_id=
user_email_address=

gcloud container attached clusters register "kind-${resource_id}" --location "asia-northeast2" \
    --fleet-project "${project_id}" --platform-version "1.27.0-gke.3" --distribution "generic" \
    --context "$( kubectl config current-context )" --has-private-issuer \
    --admin-users "${user_email_address}"
```

### 7. クラウドに kind クラスタの参照権限を付与

Connect Gateway の設定をします。  
https://cloud.google.com/anthos/multicluster-management/gateway/setup?hl=ja

クラウド側で権限を設定し、

```sh
user_email_address=

export project_id=$( gcloud config get-value project )
gcloud projects add-iam-policy-binding ${project_id} \
    --member "user:${user_email_address}" \
    --role "roles/gkehub.gatewayAdmin"
gcloud projects add-iam-policy-binding ${project_id} \
    --member "user:${user_email_address}" \
    --role "roles/gkehub.viewer"
```

Kubernetes クラスタ側でも RBAC 認可を設定します。  
kubectl が届く場所で以下を実行しましょう。

```sh
gcloud container fleet memberships generate-gateway-rbac  \
    --membership "kind-${resource_id}" \
    --users "${user_email_address}" --role "clusterrole/cluster-admin" \
    --kubeconfig $HOME/.kube/config --context "$( kubectl config current-context )" \
    --apply
```

ここまで設定したら、クラウドのコンソールから「Google ID を使用してログインします」を選択してください。

### 8. 任意の場所からクラスタを操作

```sh
gcloud container fleet memberships get-credentials "kind-${resource_id}"
kubectl get nodes
```
